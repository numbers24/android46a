package com.androidassignment.chessgamev2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.SharedMemory;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileView extends AppCompatActivity {

    public ArrayList<Record> records;
    CheckBox date, title;
    ListView filelist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hide();
        setContentView(R.layout.file_view);
        title = (CheckBox)findViewById(R.id.sortbytitle);
        date = (CheckBox)findViewById(R.id.sortbydate);
        filelist = (ListView) findViewById(R.id.fileList);

        filelist.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String,String> item = (Map<String,String>) filelist.getItemAtPosition(position);
                open(item.get("text1"));
            }
        });

        records = new ArrayList<Record>();
        getFiles();
        view(records);
    }
    private void hide() {

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }
    public void open(String t){
        for(Record r: records){
            if(t.equals(r.getTitle())){

                Intent viewFiles = new Intent(this, ReplayActivity.class);

                viewFiles.putExtra("Record",r);
                startActivity(viewFiles);
                return;
            }
        }
    }
    public void view(ArrayList<Record> records){
        String[] key = {"text1","text2"};
        int[] layout = {android.R.id.text1,android.R.id.text2};
        List<Map<String,String>> itemList = new ArrayList<Map<String,String>>();
        for(Record r : records){
            Map<String,String> item = new HashMap<String,String>();
            item.put("text1",r.getTitle());
            item.put("text2",r.getDate());
            itemList.add(Collections.unmodifiableMap(item));
        }


        SimpleAdapter adapter = new SimpleAdapter(this,itemList, android.R.layout.simple_expandable_list_item_2,key,layout);
        filelist.setAdapter(adapter);
    }
    public void getFiles() {
        try {
            final File dir = new File(this.getFilesDir(), "savedgames");
            if (!dir.exists()) dir.mkdir();

            for (String file : dir.list()) {
                Record r = new Record();
                r.read(dir + "/" + file);
                records.add(r);
            }
        } catch (Exception ioe) {
        }

    }

    public void sortbyTitle(View checkmark){
        if(date.isChecked()&&title.isChecked())
            date.toggle();
        if(!date.isChecked()&&!title.isChecked()){
            view(records);
            return;
        }

        ArrayList<Record>recordsBySort=new ArrayList<Record>();
        for(Record r: records){
            boolean done = false;
            for(Record rs : recordsBySort){
                if(r.getTitle().compareTo(rs.getTitle())<0){
                    recordsBySort.add(recordsBySort.indexOf(rs),r);
                    done=true;
                    break;
                }
            }
            if(done)continue;
            else recordsBySort.add(r);
        }
        view(recordsBySort);

    }
    public void sortbyDate(View checkmark){
        if(title.isChecked()&&date.isChecked()) title.toggle();
        if(!date.isChecked()&&!title.isChecked()){
            view(records);
            return;
        }
        ArrayList<Record> recordsBySort=new ArrayList<Record>();
        for(Record r: records){
            boolean done = false;
            for(Record rs : recordsBySort){
                if(r.getDate().compareTo(rs.getDate())>0){
                    recordsBySort.add(recordsBySort.indexOf(rs),r);
                    done=true;
                    break;
                }
            }
            if(done) continue;
            else recordsBySort.add(r);
        }
        view(recordsBySort);

    }
}
