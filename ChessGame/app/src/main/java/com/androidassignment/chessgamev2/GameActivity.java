package com.androidassignment.chessgamev2;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class GameActivity extends AppCompatActivity {
    public static int turn;
    public static boolean matchEnd;
    public static String blackKing[] = new String[]{"e8","safe"};
    public static String whiteKing[] = new String[]{"e1","safe"};
    public static boolean stalemate = false;
    public static String passant = "";
    public static boolean drawAvailable = false;
    String activeLabel;
    ArrayList<String> validMoves = new ArrayList<String>();
    ArrayList<String> list = new ArrayList<String>();
    public static String chess_game_player = "";
    public static boolean[] blackCastle = new boolean[]{true,true};
    public static boolean[] whiteCastle = new boolean[]{true,true};

    boolean firstSelection = true;
    BoardBox source = null;
    BoardBox target = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hide();
        setContentView(R.layout.game_activity);
        turn = 1;
        matchEnd = false;
        chess_game_player="";
        Board.createBoard();
    }

    private void hide() {
        
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    public void playGame(View touchedTile){
        if(matchEnd)
            return;
        if(turn % 2 == 0)
            chess_game_player = "Black";
        else
            chess_game_player = "White";

        if(firstSelection){
            source = (BoardBox) touchedTile;
            activeLabel = getResources().getResourceName(source.getId());
            
            if(source.getDrawable() == null)
                return;

            String originPoints =  moveConverter.convert(activeLabel.substring(activeLabel.length()-2));

            int originFile = Character.getNumericValue(originPoints.charAt(0));
            int originRank = Character.getNumericValue(originPoints.charAt(1));

            if(Board.board_postions[originFile][originRank].currentPiece.charAt(0) != Character.toLowerCase(chess_game_player.charAt(0))) {
                Toast.makeText(GameActivity.this, "Wrong Piece,!!!",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            validMoves = GameRules.findPossibleMoves( activeLabel.substring(activeLabel.length()-2) );
            if(validMoves.isEmpty()) {
                Toast.makeText(GameActivity.this, "Nothing is possible with that piece\nTry again...",
                        Toast.LENGTH_SHORT).show();
                return;
            }
           
            firstSelection = false;
        }
        else{
            target = (BoardBox) touchedTile;
            String startingLabel = activeLabel.substring(activeLabel.length()-2);
            activeLabel = getResources().getResourceName(target.getId());

            if(target != source){
                boolean foundMatch = false;
                for(String z: validMoves){
                    if (z.equals( activeLabel.substring(activeLabel.length()-2) ) ){
                        boolean leftKingInCheck;
                        leftKingInCheck = !MovePiece(startingLabel, activeLabel.substring(activeLabel.length()-2), "", true,0);
                        if(leftKingInCheck){
                            Toast.makeText(GameActivity.this, "Don't leave your king in check!\nTry again...",
                                    Toast.LENGTH_SHORT).show();
                            firstSelection = true;
                            return;
                        }
                        foundMatch = true;
                        break;
                    }
                }
                if(!foundMatch){
                    Toast.makeText(GameActivity.this, "Illegal move.\nTry again...",
                            Toast.LENGTH_SHORT).show();
                    firstSelection = true;
                    return;
                }

                target.setImageDrawable(source.getDrawable());
                target.currentPiece = source.currentPiece;
                source.setImageDrawable(null);
                source.currentPiece = "empty";

                //add to list
                list.add(startingLabel + " " + activeLabel.substring(activeLabel.length()-2));

                /*TODO: Need to incorporate some UI elements that indicate check and checkmate!*/
                boolean putEnemyInCheck;
                if(chess_game_player.equals("White")){
                    putEnemyInCheck = GameRules.leavesKingInCheck(blackKing[0], "b");
                    if(putEnemyInCheck){
                        blackKing[1] = "check";
                        matchEnd = GameRules.determineCheckmate(turn+1);
                        if(matchEnd){
                            Toast.makeText(GameActivity.this, "Checkmate! White wins!",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(GameActivity.this, "Check!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                        blackKing[1] = "safe";
                }
                else{
                    putEnemyInCheck = GameRules.leavesKingInCheck(whiteKing[0], "w");
                    if(putEnemyInCheck){
                        whiteKing[1] = "check";
                        matchEnd = GameRules.determineCheckmate(turn+1);
                        if(matchEnd){
                            Toast.makeText(GameActivity.this, "Checkmate! Black wins!",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(GameActivity.this, "Check!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                        whiteKing[1] = "safe";
                }

                //support for our castling implementation?

                /*TODO: Might be possible to implement undo button here*/
                ++turn;
                firstSelection = true;

                if(matchEnd){

                    String fullList = "Here are the recorded moves: \n";
                    for(String x : list){
                        fullList += x;
                        fullList += ",\t";
                    }
                    Toast.makeText(GameActivity.this, fullList,
                            Toast.LENGTH_LONG).show();

                    try {
                        FileOutputStream fos = new FileOutputStream("List of moves");
                        ObjectOutputStream oos = new ObjectOutputStream(fos);
                        oos.writeObject(list);
                        File f = new File(".");
                        Toast.makeText(GameActivity.this, "Moves have been saved.\nFile path = " + f.getAbsolutePath(),
                                Toast.LENGTH_LONG).show();
                        oos.close();
                        fos.close();

                    } catch (Exception ioe) {
                        ioe.printStackTrace();
                        Toast.makeText(GameActivity.this, "failed to write game data",
                                Toast.LENGTH_LONG).show();
                    }

                }
                return;
            }
            
            Toast.makeText(GameActivity.this, "Move canceled...",
                    Toast.LENGTH_SHORT).show();
            firstSelection = true;
        }
    }

   
    public static boolean MovePiece(String origin, String destination, String promo, boolean keepIt, int castling){
        origin = moveConverter.convert(origin);
        int originFile = Character.getNumericValue(origin.charAt(0));
        int originRank = Character.getNumericValue(origin.charAt(1));
        String originPiece = Board.board_postions[originFile][originRank].currentPiece;

        destination = moveConverter.convert(destination);
        int destinationFile = Character.getNumericValue(destination.charAt(0));
        int destinationRank = Character.getNumericValue(destination.charAt(1));
        String destinationPiece = Board.board_postions[destinationFile][destinationRank].currentPiece;

        Board.board_postions[destinationFile][destinationRank].currentPiece = originPiece;
        Board.board_postions[originFile][originRank].currentPiece = "empty";

        String previousWhiteKing = whiteKing[0];
        String previousBlackKing = blackKing[0];

        if(originPiece.equals("wK"))
            whiteKing[0] = Board.board_postions[destinationFile][destinationRank].label;
        if(originPiece.equals("bK"))
            blackKing[0] = Board.board_postions[destinationFile][destinationRank].label;

        int rookOriginFile, rookOriginRank, rookDestFile, rookDestRank;
        switch(castling){
            case 4:
                rookOriginFile = 1;
                rookOriginRank = 1;
                rookDestFile = 1;
                rookDestRank = 3;
                break;
            case 3:
                rookOriginFile = 1;
                rookOriginRank = 8;
                rookDestFile = 1;
                rookDestRank = 5;
                break;
            case 2:
                rookOriginFile = 8;
                rookOriginRank = 1;
                rookDestFile = 8;
                rookDestRank = 3;
                break;
            case 1:
                rookOriginFile = 8;
                rookOriginRank = 8;
                rookDestFile = 8;
                rookDestRank = 5;
                break;
            default:
                rookOriginFile = 0;
                rookOriginRank = 0;
                rookDestFile = 0;
                rookDestRank = 0;
        }

        if(rookOriginFile+rookOriginRank+rookDestFile+rookDestRank > 0){
            Board.board_postions[rookDestFile][rookDestRank].currentPiece = Board.board_postions[rookOriginFile][rookOriginRank].currentPiece;
            Board.board_postions[rookOriginFile][rookOriginRank].currentPiece = "empty";
        }

        String thisKing = "";
        if(originPiece.substring(0,1).equalsIgnoreCase("b"))
            thisKing = blackKing[0];
        else
            thisKing = whiteKing[0];

        if(GameRules.leavesKingInCheck(thisKing, originPiece.substring(0,1) ) ){
            Board.board_postions[destinationFile][destinationRank].currentPiece = destinationPiece;
            Board.board_postions[originFile][originRank].currentPiece = originPiece;
            if(rookOriginFile+rookOriginRank+rookDestFile+rookDestRank > 0){
                Board.board_postions[rookOriginFile][rookOriginRank].currentPiece = Board.board_postions[rookDestFile][rookDestRank].currentPiece;
                Board.board_postions[rookDestFile][rookDestRank].currentPiece = "empty";
            }
            whiteKing[0] = previousWhiteKing;
            blackKing[0] = previousBlackKing;
            return false;
        }
        if(!keepIt){
            Board.board_postions[destinationFile][destinationRank].currentPiece = destinationPiece;
            Board.board_postions[originFile][originRank].currentPiece = originPiece;
            if(rookOriginFile+rookOriginRank+rookDestFile+rookDestRank > 0){
                Board.board_postions[rookOriginFile][rookOriginRank].currentPiece = Board.board_postions[rookDestFile][rookDestRank].currentPiece;
                Board.board_postions[rookDestFile][rookDestRank].currentPiece = "empty";
            }
            whiteKing[0] = previousWhiteKing;
            blackKing[0] = previousBlackKing;
            return true;
        }

        switch(castling){
            case 4:
                whiteCastle[0] = false;
                whiteCastle[1] = false;
                break;
            case 3:
                whiteCastle[1] = false;
                whiteCastle[0] = false;
                break;
            case 2:
                blackCastle[1] = false;
                blackCastle[0] = false;
                break;
            case 1:
                blackCastle[1] = false;
                blackCastle[0] = false;
                break;
        }

        if(originPiece.equals("bp") && (destinationFile == 1 || destinationFile == '1')){

            if(promo != ""){
                Board.board_postions[destinationFile][destinationRank].currentPiece = "b" + promo;
            }
            else
                Board.board_postions[destinationFile][destinationRank].currentPiece = "bQ";
        }
        if(originPiece.equals("wp") && (destinationFile == 8 || destinationFile == '8')){

            if(promo != ""){
                Board.board_postions[destinationFile][destinationRank].currentPiece = "w" + promo;
            }
            else
                Board.board_postions[destinationFile][destinationRank].currentPiece = "wQ";
        }

        if(originPiece.charAt(1) == 'p' && Math.abs(destinationFile - originFile) == 2)
            passant = destination;
        else
            passant = "";

        return true;
    }
}
