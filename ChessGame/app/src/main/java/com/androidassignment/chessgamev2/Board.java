package com.androidassignment.chessgamev2;

public class Board {
   
    public static Position[][] board_postions = new Position[9][9];

    public static void createBoard() {
        for (int i = 8; i > -1; --i) {
            for (int j = 8; j > -1; --j) {
                board_postions[i][j] = new Position();

              
                char file = 96;
                switch (j) {
                    case 8:
                        file += 1;
                        break;
                    case 7:
                        file += 2;
                        break;
                    case 6:
                        file += 3;
                        break;
                    case 5:
                        file += 4;
                        break;
                    case 4:
                        file += 5;
                        break;
                    case 3:
                        file += 6;
                        break;
                    case 2:
                        file += 7;
                        break;
                    case 1:
                        file += 8;
                        break;
                    case 0:
                        file = '|';
                        break;
                }
                board_postions[i][j].label = Character.toString(file) + Integer.toString(i);

                if ((i + j) % 2 == 0)
                    board_postions[i][j].defaultColor = "     ";
            }
        }

        board_postions[1][1].currentPiece = "wR";
        board_postions[1][8].currentPiece = "wR";
        board_postions[8][1].currentPiece = "bR";
        board_postions[8][8].currentPiece = "bR";

        board_postions[1][2].currentPiece = "wN";
        board_postions[1][7].currentPiece = "wN";
        board_postions[8][2].currentPiece = "bN";
        board_postions[8][7].currentPiece = "bN";

        board_postions[1][3].currentPiece = "wB";
        board_postions[1][6].currentPiece = "wB";
        board_postions[8][3].currentPiece = "bB";
        board_postions[8][6].currentPiece = "bB";

        board_postions[1][5].currentPiece = "wQ";
        board_postions[1][4].currentPiece = "wK";
        board_postions[8][5].currentPiece = "bQ";
        board_postions[8][4].currentPiece = "bK";

        for (int x = 1; x < 9; x++) {
            board_postions[2][x].currentPiece = "wp";
        }
        for (int x = 1; x < 9; x++) {
            board_postions[7][x].currentPiece = "bp";
        }
    }
}
