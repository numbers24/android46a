package com.androidassignment.chessgamev2;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class ChessMoves extends AppCompatActivity {
    public static int turn;
    public static boolean matchEnd;
    public static String blackKing[] = new String[]{"e8","safe"};
    public static String whiteKing[] = new String[]{"e1","safe"};
    public static boolean stalemate = false;
    public static String passant = "";
    public static boolean drawAvailable = false;
    String activeLabel;
    ArrayList<String> validMoves = new ArrayList<String>();
    ArrayList<String> list = new ArrayList<String>();
    public static String chess_game_player = "";
    public static boolean[] blackCastle = new boolean[]{true,true};
    public static boolean[] whiteCastle = new boolean[]{true,true};

    boolean firstSelection = true;
    BoardBox source = null;
    BoardBox target = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hide();
        setContentView(R.layout.game_activity);
        turn = 1;
        matchEnd = false;
        chess_game_player="";
        Board.createBoard();
    }

    private void hide() {
        
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    public void resign(View touchedTile){
        String winner;
        if(turn % 2 == 0){
            chess_game_player = "Black";
            winner = "White";
        }
        else{
            chess_game_player = "White";
            winner="Black";
        }
        Toast.makeText(ChessMoves.this, chess_game_player+" has resigned. "+winner+" wins!!!",
                Toast.LENGTH_SHORT).show();
        list.add("resign");
        endMatch();
    }

    boolean draw=false;
    public void draw(View touchedTile){
        list.add("draw");
        if(turn % 2 == 0){
            chess_game_player = "Black";
        }
        else{
            chess_game_player = "White";
        }
        if(!draw){
            Toast.makeText(ChessMoves.this, chess_game_player+" has requested a draw.",
                    Toast.LENGTH_SHORT).show();
            draw=true;
        }
        else{
            Toast.makeText(ChessMoves.this, chess_game_player+" has accepted the draw.",
                    Toast.LENGTH_SHORT).show();
            endMatch();
        }
        turn++;
    }
    String [] moveset = {
            "a1","a2","a3","a4","a5","a6","a7","a8",
            "b1","b2","b3","b4","b5","b6","b7","b8",
            "c1","c2","c3","c4","c5","c6","c7","c8",
            "d1","d2","d3","d4","d5","d6","d7","d8",
            "e1","e2","e3","e4","e5","e6","e7","e8",
            "f1","f2","f3","f4","f5","f6","f7","f8",
            "g1","g2","g3","g4","g5","g6","g7","g8",
            "h1","h2","h3","h4","h5","h6","h7","h8",
            "draw","resign"
    };
    public void ai(View touchedTile){
        String origin = moveset[(int)Math.random()*moveset.length];
        switch (origin){
            case "draw": draw(touchedTile); break;
            case "resign": resign(touchedTile); break;
            default:
                String destination = moveset[(int)Math.random()*moveset.length];
        }

    }
    public void playGame(View touchedTile){

        if(matchEnd)
            return;
        if(turn % 2 == 0)
            chess_game_player = "Black";
        else
            chess_game_player = "White";

        if(draw){
            Toast.makeText(ChessMoves.this, chess_game_player+" has rejected the draw!",
                    Toast.LENGTH_SHORT).show();
            draw=false;
        }

        if(firstSelection){
            source = (BoardBox) touchedTile;
            activeLabel = getResources().getResourceName(source.getId());
            
            if(source.getDrawable() == null)
                return;

            String originPoints =  moveConverter.convert(activeLabel.substring(activeLabel.length()-2));

            int originFile = Character.getNumericValue(originPoints.charAt(0));
            int originRank = Character.getNumericValue(originPoints.charAt(1));

            if(Board.board_postions[originFile][originRank].currentPiece.charAt(0) != Character.toLowerCase(chess_game_player.charAt(0))) {
                Toast.makeText(ChessMoves.this, "That's a Wrong Piece,!!!",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            validMoves = ChessBoardRules.getPossibleMoves( activeLabel.substring(activeLabel.length()-2) );
            if(validMoves.isEmpty()) {
                Toast.makeText(ChessMoves.this, "Unfortuently Nothing is possible with that piece\nTry again...",
                        Toast.LENGTH_SHORT).show();
                return;
            }
           
            firstSelection = false;
        }
        else{
            target = (BoardBox) touchedTile;
            String startingLabel = activeLabel.substring(activeLabel.length()-2);
            activeLabel = getResources().getResourceName(target.getId());

            if(target != source){
                boolean foundMatch = false;
                for(String z: validMoves){
                    if (z.equals( activeLabel.substring(activeLabel.length()-2) ) ){
                        boolean leftKingInCheck;
                        leftKingInCheck = !MovePiece(startingLabel, activeLabel.substring(activeLabel.length()-2), "", true,0);
                        if(leftKingInCheck){
                            Toast.makeText(ChessMoves.this, "Please Don't leave your king in check!\nTry again...",
                                    Toast.LENGTH_SHORT).show();
                            firstSelection = true;
                            return;
                        }
                        foundMatch = true;
                        break;
                    }
                }
                if(!foundMatch){
                    Toast.makeText(ChessMoves.this, "Invalid Move move.\nTry again...",
                            Toast.LENGTH_SHORT).show();
                    firstSelection = true;
                    return;
                }

                target.setImageDrawable(source.getDrawable());
                target.currentPiece = source.currentPiece;
                source.setImageDrawable(null);
                source.currentPiece = "empty";

                //add to list
                list.add(startingLabel + " " + activeLabel.substring(activeLabel.length()-2));

                /*TODO: TO ADD*/
                boolean putEnemyInCheck;
                if(chess_game_player.equals("White")){
                    putEnemyInCheck = ChessBoardRules.leavesKingInCheck(blackKing[0], "b");
                    if(putEnemyInCheck){
                        blackKing[1] = "check";
                        matchEnd = ChessBoardRules.determineCheckmate(turn+1);
                        if(matchEnd){
                            Toast.makeText(ChessMoves.this, "Checkmate! White wins!",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(ChessMoves.this, "Check!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                        blackKing[1] = "safe";
                }
                else{
                    putEnemyInCheck = ChessBoardRules.leavesKingInCheck(whiteKing[0], "w");
                    if(putEnemyInCheck){
                        whiteKing[1] = "check";
                        matchEnd = ChessBoardRules.determineCheckmate(turn+1);
                        if(matchEnd){
                            Toast.makeText(ChessMoves.this, "Checkmate! Black wins!",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(ChessMoves.this, "Check!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                        whiteKing[1] = "safe";
                }


                /*TODO: ADD UNTO BUTTON HERE*/
                Button undo = (Button) findViewById(R.id.undo);
                undo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });


                ++turn;
                firstSelection = true;

                if(matchEnd){
                    endMatch();
                }
                return;
            }
            
            Toast.makeText(ChessMoves.this, "Move canceled...",
                    Toast.LENGTH_SHORT).show();
            firstSelection = true;
        }
    }

   
    public static boolean MovePiece(String origin, String destination, String promo, boolean keepIt, int castling){
        origin = moveConverter.convert(origin);
        int originFile = Character.getNumericValue(origin.charAt(0));
        int originRank = Character.getNumericValue(origin.charAt(1));
        String originPiece = Board.board_postions[originFile][originRank].currentPiece;

        destination = moveConverter.convert(destination);
        int destinationFile = Character.getNumericValue(destination.charAt(0));
        int destinationRank = Character.getNumericValue(destination.charAt(1));
        String destinationPiece = Board.board_postions[destinationFile][destinationRank].currentPiece;

        Board.board_postions[destinationFile][destinationRank].currentPiece = originPiece;
        Board.board_postions[originFile][originRank].currentPiece = "empty";

        String previousWhiteKing = whiteKing[0];
        String previousBlackKing = blackKing[0];

        if(originPiece.equals("wK"))
            whiteKing[0] = Board.board_postions[destinationFile][destinationRank].label;
        if(originPiece.equals("bK"))
            blackKing[0] = Board.board_postions[destinationFile][destinationRank].label;

        int rookOriginFile, rookOriginRank, rookDestFile, rookDestRank;
        switch(castling){
            case 4:
                rookOriginFile = 1;
                rookOriginRank = 1;
                rookDestFile = 1;
                rookDestRank = 3;
                break;
            case 3:
                rookOriginFile = 1;
                rookOriginRank = 8;
                rookDestFile = 1;
                rookDestRank = 5;
                break;
            case 2:
                rookOriginFile = 8;
                rookOriginRank = 1;
                rookDestFile = 8;
                rookDestRank = 3;
                break;
            case 1:
                rookDestFile = 8;
                rookDestRank = 5;
                rookOriginFile = 8;
                rookOriginRank = 8;

                break;
            default:
                rookDestFile = 0;
                rookDestRank = 0;
                rookOriginFile = 0;
                rookOriginRank = 0;

        }

        if(rookOriginFile+rookOriginRank+rookDestFile+rookDestRank > 0){
            Board.board_postions[rookDestFile][rookDestRank].currentPiece = Board.board_postions[rookOriginFile][rookOriginRank].currentPiece;
            Board.board_postions[rookOriginFile][rookOriginRank].currentPiece = "empty";
        }

        String thisKing = "";
        if(originPiece.substring(0,1).equalsIgnoreCase("b"))
            thisKing = blackKing[0];
        else
            thisKing = whiteKing[0];

        if(ChessBoardRules.leavesKingInCheck(thisKing, originPiece.substring(0,1) ) ){
            Board.board_postions[destinationFile][destinationRank].currentPiece = destinationPiece;
            Board.board_postions[originFile][originRank].currentPiece = originPiece;
            if(rookOriginFile+rookOriginRank+rookDestFile+rookDestRank > 0){
                Board.board_postions[rookOriginFile][rookOriginRank].currentPiece = Board.board_postions[rookDestFile][rookDestRank].currentPiece;
                Board.board_postions[rookDestFile][rookDestRank].currentPiece = "empty";
            }
            whiteKing[0] = previousWhiteKing;
            blackKing[0] = previousBlackKing;
            return false;
        }
        if(!keepIt){
            Board.board_postions[destinationFile][destinationRank].currentPiece = destinationPiece;
            Board.board_postions[originFile][originRank].currentPiece = originPiece;
            if(rookOriginFile+rookOriginRank+rookDestFile+rookDestRank > 0){
                Board.board_postions[rookOriginFile][rookOriginRank].currentPiece = Board.board_postions[rookDestFile][rookDestRank].currentPiece;
                Board.board_postions[rookDestFile][rookDestRank].currentPiece = "empty";
            }
            whiteKing[0] = previousWhiteKing;
            blackKing[0] = previousBlackKing;
            return true;
        }

        switch(castling){
            case 4:
                whiteCastle[0] = false;
                whiteCastle[1] = false;
                break;
            case 3:
                whiteCastle[1] = false;
                whiteCastle[0] = false;
                break;
            case 2:
                blackCastle[1] = false;
                blackCastle[0] = false;
                break;
            case 1:
                blackCastle[1] = false;
                blackCastle[0] = false;
                break;
        }

        if(originPiece.equals("bp") && (destinationFile == 1 || destinationFile == '1')){

            if(promo != ""){
                Board.board_postions[destinationFile][destinationRank].currentPiece = "b" + promo;
            }
            else
                Board.board_postions[destinationFile][destinationRank].currentPiece = "bQ";
        }
        if(originPiece.equals("wp") && (destinationFile == 8 || destinationFile == '8')){

            if(promo != ""){
                Board.board_postions[destinationFile][destinationRank].currentPiece = "w" + promo;
            }
            else
                Board.board_postions[destinationFile][destinationRank].currentPiece = "wQ";
        }

        if(originPiece.charAt(1) == 'p' && Math.abs(destinationFile - originFile) == 2)
            passant = destination;
        else
            passant = "";

        return true;
    }


    public void endMatch(){
        String fullList = "View Recorded Moves: \n";
        for(String x : list){
            fullList += x;
            fullList += ",\t";
        }
        Toast.makeText(ChessMoves.this, fullList,
                Toast.LENGTH_LONG).show();


        final File dir = new File(this.getFilesDir(),"savedgames");
        if(!dir.exists()) dir.mkdir();
        LayoutInflater li = LayoutInflater.from(this);
        View save_prompt = li.inflate(R.layout.save_prompt,null);
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setView(save_prompt);
        final EditText UI = (EditText) save_prompt.findViewById(R.id.editTextDialogUserInput);

        adb.setCancelable(false)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String title = UI.getText().toString();
                        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                        Date date = new Date();
                        try {
                            Record game = new Record(title, sdf.format(date), list);
                            game.write(dir);

                            Toast.makeText(ChessMoves.this, "Saved as " + title + "\nAt: " + game.getAbsolutePath(dir),
                                    Toast.LENGTH_LONG).show();

                        } catch (Exception ioe) {
                            ioe.printStackTrace();
                            Toast.makeText(ChessMoves.this, "FAILED",
                                    Toast.LENGTH_LONG).show();
                        }
                        //go home
                    }
                })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            Toast.makeText(ChessMoves.this, "Game Not Saved",
                                    Toast.LENGTH_LONG).show();
                            //go home
                        }
                    });

        AlertDialog ad = adb.create();
        ad.show();


    }
}
