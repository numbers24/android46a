package com.androidassignment.chessgamev2;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ReplayActivity extends AppCompatActivity {

    Record record;
    int count;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hide();
        setContentView(R.layout.replay_activity);
        count = 0;
        record = (Record)getIntent().getSerializableExtra("Record");

    }

    private void hide() {

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }
    boolean pause=true;

    public void play(View view){
        pause=!pause;
        if(!pause){
            for(;count<record.getList().size();count++){
                //run the command through chessmoves
                String move = record.getList().get(count);

            }
        }
        else{
            Toast.makeText(this, "Game Paused",
                    Toast.LENGTH_SHORT).show();
        }
    }
    public void next(View view){
        if(count<record.getList().size()){
            String move = record.getList().get(count++);
        }
    }
}