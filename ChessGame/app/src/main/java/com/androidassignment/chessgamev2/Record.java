package com.androidassignment.chessgamev2;

import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

public class Record implements Serializable {

    String title;
    String date;
    ArrayList<String> list;

    public Record(String title, String date, ArrayList<String> list){
        this.title = title;
        this.date = date;
        this.list = list;
    }
    public Record(){}

    public void write(File dir) throws Exception {
        File f = new File(dir,title+".txt");
        if(f.exists()){
            //count them up
        }
        FileWriter fw = new FileWriter(f);
        fw.write(title+"\n"+date+"\n");
        for(String s : list)
            fw.write(s+"\n");
        fw.close();
    }

    public File getAbsolutePath(File dir){
        return new File(dir,title+".txt");
    }
    public String getTitle(){return title;}
    public String getDate(){return date;}
    public ArrayList<String> getList(){return list;}

    public void read(String dir) throws Exception {
        list = new ArrayList<String>();
        Scanner sc = new Scanner(new File(dir));
        title = sc.nextLine();
        date = sc.nextLine();
        while (sc.hasNextLine()){
            list.add(sc.nextLine());
        }
        sc.close();

    }

}
