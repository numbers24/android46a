package com.androidassignment.chessgamev2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;


@SuppressLint("AppCompatCustomView")
public class BoardBox extends ImageView {
    public String currentPiece;
    public String label;

    public BoardBox(Context givenContext) {
        super(givenContext);
        this.currentPiece = "empty";
        this.label = "nowhere";
    }

    public BoardBox(Context givenContext, AttributeSet givenAttrs) {
        super(givenContext, givenAttrs);
        this.currentPiece = "empty";
        this.label = "nowhere";
    }

    public BoardBox(Context givenContext, AttributeSet givenAttrs, int givenStyleAttr) {
        super(givenContext, givenAttrs, givenStyleAttr);
        this.currentPiece = "empty";
        this.label = "nowhere";
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BoardBox(Context givenContext, AttributeSet givenAttrs, int givenStyleAttr, int givenStyleRes) {
        super(givenContext, givenAttrs, givenStyleAttr, givenStyleRes);
        this.currentPiece = "empty";
        this.label = "nowhere";
    }
}