package com.androidassignment.chessgamev2;

import java.util.ArrayList;

public class ChessBoardRules {
    
    public static boolean determineCheckmate(int nextTurn){
		
        String player_two_color = "";
        int start_point=0, end_point=0, iterator=0;

        switch(nextTurn % 2){
            case 0:
                player_two_color = "b";
                start_point = 8;
                end_point = 0;
                iterator = -1;
                break;
            case 1:
                player_two_color = "w";
                start_point = 1;
                end_point = 9;
                iterator = 1;
                break;
            default:
                System.out.println("You are not counting turns correctly!");
        }

        for(int i = start_point; i!=end_point; i+=iterator){
            
            for(int j = start_point; j!=end_point; j+=iterator){
                if(Board.board_postions[i][j].currentPiece.substring(0,1).equalsIgnoreCase(player_two_color) ){
                    String origin = Board.board_postions[i][j].label;
                    
                    ArrayList<String> possibleMoves = getPossibleMoves(origin);
                    
                    for(String move : possibleMoves){
                        if(ChessMoves.MovePiece(origin,move,"",false,0))
                            return false;
                    }
                }
            }
        }

        return true;
    }


    public static ArrayList<String> getPossibleMoves (String currentTile){
        String location = moveConverter.convert(currentTile);
        int rank = Character.getNumericValue(location.charAt(0));
        int file = Character.getNumericValue(location.charAt(1));


        ArrayList<String> moves = new ArrayList <String>();
        String piece = Board.board_postions[rank][file].currentPiece;

        if(piece.equals("empty"))
            return moves;
        char currentColor = piece.charAt(0);

        switch (piece.charAt(1)){
			/*The procedure for each piece will be as follows:
			 * for(every direction the piece can move in)
			 * 	if this square is empty,
			 * 		add it to the validMoves list
			 * 		check the next square
			 * 	else (this square is occupied)
			 * 		if the occupying piece is of the enemy color
			 * 			add this square to the validMoves list
			 * 			check the next square
			 * 		stop*/

            case 'p':

                int start_pointingRow = 0;
                int sideOfBoard = 0;

                if(currentColor == 'b'){
                    start_pointingRow = 7;
                    sideOfBoard = -1;
                }
                else{
                    start_pointingRow = 2;
                    sideOfBoard = 1;
                }

                Position possible = Board.board_postions[rank+sideOfBoard][file];
                if(possible.currentPiece.equals("empty"))
                    moves.add(possible.label);

                if(rank == start_pointingRow ){
                    possible = Board.board_postions[rank+(2*sideOfBoard)][file];
                    //if the pawn is still in it's color's start_pointing row, it can moves two spaces if that tile isn't occupied
                    if(possible.currentPiece.equals("empty"))
                        moves.add(possible.label);
                }

                for(int x=-1;  x<2; x+=2){
                    int row = rank+sideOfBoard;
                    int column = file+x;
                    if(row > 8 || row < 0 || column > 8 || column < 0)
                        continue;
                    possible = Board.board_postions[row][column];
                    if(possible.currentPiece.equals("empty")){
                        //en passant only ever happens when a pawn is 3 spaces past its home row
                        if(rank == (start_pointingRow+(3*sideOfBoard) ) && Board.board_postions[rank][column].label.equals(ChessMoves.passant)){
                            //en passant implementation
                            moves.add(possible.label);
                        }
                        else
                            continue;
                    }
                    else{
                        if(possible.currentPiece.charAt(0) != currentColor)
                            moves.add(possible.label);
                        continue;
                    }
                }
                break;

            case 'N':

                for(int i=-2; i<3; ++i){
                    for(int j=-2; j<3; ++j){
                        //filters out any board_postions that don't match the knight's pattern of movement
                        if(Math.abs(i) == Math.abs(j) || j==0 || i==0)
                            continue;
                        //make sure we stay within the boundaries of the board!
                        int x = rank+i;
                        int y = file+j;
                        Position target = new Position();
                        if(x < 9 && x > 0 && y < 9 && y > 0)
                            target = Board.board_postions[x][y];
                        else
                            continue;
                        if(target.currentPiece.equals("empty")){
                            moves.add(target.label);
                            continue;
                        }
                        else{
                            if(target.currentPiece.charAt(0) != currentColor)
                                moves.add(target.label);
                            continue;
                        }
                    }
                }
                break;

            case 'B':
                
                for(int i=-1; i<2; i+=2){
                    for(int j=-1; j<2; j+=2){

                        int x = rank+i;
                        int y = file+j;
                        Position target = new Position();

                        if(x < 9 && x > 0 && y < 9 && y > 0)
                            target = Board.board_postions[x][y];
                        else
                            continue;

                        if(target.currentPiece.equals("empty")){
                            moves.add(target.label);
                            x+=i;
                            y+=j;
                            while(x < 9 && x > 0 && y < 9 && y > 0){
                                target = Board.board_postions[x][y];
                                if(target.currentPiece.equals("empty")){
                                    moves.add(target.label);
                                    x = x+i;
                                    y = y+j;
                                    continue;
                                }
                                else{
                                    if(target.currentPiece.charAt(0) != currentColor)
                                        moves.add(target.label);
                                    break;
                                }
                            }
                        }
                        else{
                            if(target.currentPiece.charAt(0) != currentColor)
                                moves.add(target.label);
                            continue;
                        }
                    }
                }
                break;

            case 'Q':

                for(int i=-1; i<2; ++i){
                    for(int j=-1; j<2; ++j){
                        //exclude the current tile
                        if(j==0 && i==0)
                            continue;

                        int x = rank+i;
                        int y = file+j;
                        Position target = new Position();

                        if(x < 9 && x > 0 && y < 9 && y > 0)
                            target = Board.board_postions[x][y];
                        else
                            continue;
                        if(target.currentPiece.equals("empty")){
                            moves.add(target.label);
                            x+=i;
                            y+=j;
                            while(x < 9 && x > 0 && y < 9 && y > 0){
                                target = Board.board_postions[x][y];
                                if(target.currentPiece.equals("empty")){
                                    moves.add(target.label);
                                    x = x+i;
                                    y = y+j;
                                    continue;
                                }
                                else{
                                    if(target.currentPiece.charAt(0) != currentColor)
                                        moves.add(target.label);
                                    break;
                                }
                            }
                        }//end_point if
                        else{
                            if(target.currentPiece.charAt(0) != currentColor)
                                moves.add(target.label);
                            continue;
                        }
                    } //end_point loop of j
                } //end_point loop of i
                break;

            case 'R':

                for(int i=-1; i<2; ++i){
                    for(int j=-1; j<2; ++j){

                        if( (j ^ i) == 0)
                            continue;
                        int x = rank+i;
                        int y = file+j;
                        Position target = new Position();

                        if(x < 9 && x > 0 && y < 9 && y > 0)
                            target = Board.board_postions[x][y];
                        else
                            continue;

                        if(target.currentPiece.equals("empty")){
                            moves.add(target.label);
                            x+=i;
                            y+=j;
                            while(x < 9 && x > 0 && y < 9 && y > 0){
                                target = Board.board_postions[x][y];
                                if(target.currentPiece.equals("empty")){
                                    moves.add(target.label);
                                    x = x+i;
                                    y = y+j;
                                    continue;
                                }
                                else{
                                    if(target.currentPiece.charAt(0) != currentColor)
                                        moves.add(target.label);
                                    break;
                                }
                            }
                        }
                        else{
                            if(target.currentPiece.charAt(0) != currentColor)
                                moves.add(target.label);
                            continue;
                        }
                    }
                }
                break;

            case 'K':

                for(int i=-1; i<2; ++i){
                    for(int j=-1; j<2; ++j){
                        //exclude the current tile
                        if(j==0 && i==0)
                            continue;

                        int x = rank+i;
                        int y = file+j;
                        Position target = new Position();
                        if(x < 9 && x > 0 && y < 9 && y > 0)
                            target = Board.board_postions[x][y];
                        else
                            continue;
                        if(target.currentPiece.equals("empty")){
                            moves.add(target.label);
                            continue;
                        }
                        else{
                            if(target.currentPiece.charAt(0) != currentColor)
                                moves.add(target.label);
                            continue;
                        }
                    }
                }


				
                boolean queenSideCastle = false;
                boolean kingSideCastle = false;

                if (currentColor == 'b'){
                    kingSideCastle = ChessMoves.blackCastle[1];
                    queenSideCastle = ChessMoves.blackCastle[0];
                }
                else{
                    kingSideCastle = ChessMoves.whiteCastle[1];
                    queenSideCastle = ChessMoves.whiteCastle[0];
                }
                if(kingSideCastle){
                    for(int x=1; x<3; ++x){
                        if(file-x < 0 || file-x > 8)
                            continue;
                        Position target = Board.board_postions[rank][file-x];
                        if(!target.currentPiece.equals("empty")){
                            kingSideCastle = false;
                            break;
                        }
                    }
                    if(kingSideCastle){
                        moves.add(Board.board_postions[rank][file-2].label);
                    }
                }
                if(queenSideCastle){
                    for(int x=1; x<4; ++x){
                        if(file+x < 0 || file+x > 8)
                            continue;
                        Position target = Board.board_postions[rank][file+x];
                        if(!target.currentPiece.equals("empty")){
                            queenSideCastle = false;
                            break;
                        }
                    }
                    if(queenSideCastle){
                        moves.add(Board.board_postions[rank][file+2].label);
                    }
                }
                break;

            default:
                System.out.println("You gave is 'getPossibleMoves' an invalid piece!");
                return moves;
        }

        return moves;
    }


    public static boolean leavesKingInCheck (String king, String kingColor){
        String kingLocation = moveConverter.convert(king);
        int file = Character.getNumericValue(kingLocation.charAt(0));
        int rank = Character.getNumericValue(kingLocation.charAt(1));

        ArrayList<String> possibleAttacks = new ArrayList<String>();
        String[] attackers = new String[]{"N","B","Q","R","K"};
        for(String sampleAttacker : attackers){
            
            Board.board_postions[file][rank].currentPiece = kingColor+sampleAttacker;
            possibleAttacks = getPossibleMoves(king);
            
            for(String move : possibleAttacks){
                String attackerLocation = moveConverter.convert(move);
                int attackerFile = Character.getNumericValue(attackerLocation.charAt(0));
                int attackerRank = Character.getNumericValue(attackerLocation.charAt(1));
                
                if(Board.board_postions[attackerFile][attackerRank].currentPiece.substring(1,2).equalsIgnoreCase(sampleAttacker)){
                    Board.board_postions[file][rank].currentPiece = kingColor+"K";
                    return true;
                }
            }
          
        }
        Board.board_postions[file][rank].currentPiece = kingColor+"K";

        String attackerColor = "";
        if (kingColor.equals("b")) {
            attackerColor = "w";
        } else {
            attackerColor = "b";
        }

      

        if (file == 8 && rank == 8 && file < 9 && rank > 0) {
            return Board.board_postions[file - 1][rank - 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file - 1][rank - 1].label).contains(Board.board_postions[file][rank].label) == true);
            //king is at top right
        } else if (file == 8 && rank == 1 && file < 9 && rank > 0) {
            return Board.board_postions[file - 1][rank + 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file - 1][rank + 1].label).contains(Board.board_postions[file][rank].label));
            //king is at bottom left
        } else if (file == 1 && rank == 8 && file < 9 && rank > 0) {
            return Board.board_postions[file + 1][rank - 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file + 1][rank - 1].label).contains(Board.board_postions[file][rank].label) == true);
            //king is at bottom right
        } else if (file == 1 && rank == 1 && file < 9 && rank > 0) {
            return Board.board_postions[file + 1][rank + 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file + 1][rank + 1].label).contains(Board.board_postions[file][rank].label) == true);
            //king is at the top
        } else if (file == 8 && rank <= 7 && rank >= 2 && file < 9 && rank > 0) {
            if ((Board.board_postions[file-1][rank+1].currentPiece.equals(attackerColor+"p") && (getPossibleMoves(Board.board_postions[file-1][rank+1].label).contains(Board.board_postions[file][rank].label) == true)) || (Board.board_postions[file-1][rank-1].currentPiece.equals(attackerColor+"p") && (getPossibleMoves(Board.board_postions[file-1][rank-1].label).contains(Board.board_postions[file][rank].label) == true))) {
                System.out.println(file + " " + rank);
                return true;
            }
            //if king is at the bottom
        } else if (file == 1 && rank <= 7 && rank >= 2 && file < 9 && rank > 0) {

            return (Board.board_postions[file + 1][rank + 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file + 1][rank + 1].label).contains(Board.board_postions[file][rank].label) == true)) || (Board.board_postions[file + 1][rank - 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file + 1][rank - 1].label).contains(Board.board_postions[file][rank].label) == true));
            //if king is on the left side
        } else if (rank == 8 && file <= 7 && file >= 2 && file < 9 && rank > 0) {
            return (Board.board_postions[file + 1][rank - 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file + 1][rank - 1].label).contains(Board.board_postions[file][rank].label) == true)) || (Board.board_postions[file - 1][rank - 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file - 1][rank - 1].label).contains(Board.board_postions[file][rank].label) == true));
            //if king is on the right side
        } else if (rank == 1 && file <= 7 && file >= 2 && file < 9 && rank > 0) {
            return (Board.board_postions[file + 1][rank + 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file + 1][rank + 1].label).contains(Board.board_postions[file][rank].label) == true)) || (Board.board_postions[file - 1][rank + 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file - 1][rank + 1].label).contains(Board.board_postions[file][rank].label) == true));
            //if king is anywhere else
        } else return (Board.board_postions[file + 1][rank + 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file + 1][rank + 1].label).contains(Board.board_postions[file][rank].label) == true)) || (Board.board_postions[file + 1][rank - 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file + 1][rank - 1].label).contains(Board.board_postions[file][rank].label) == true)) || (Board.board_postions[file - 1][rank + 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file - 1][rank + 1].label).contains(Board.board_postions[file][rank].label) == true)) || (Board.board_postions[file - 1][rank - 1].currentPiece.equals(attackerColor + "p") && (getPossibleMoves(Board.board_postions[file - 1][rank - 1].label).contains(Board.board_postions[file][rank].label) == true));

        //at this point, the King must be completely safe!
        return false;
    }


    public static boolean isStalemate(){
        //to be completed
        return false;
    }
}
