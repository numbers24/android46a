package com.androidassignment.chessgamev2;


public class Position {
    public String currentPiece;
    public String defaultColor;
    public String label;

    public Position(){
        this.currentPiece="empty";
        this.defaultColor="## ";
        this.label="nowhere";
    }
}
